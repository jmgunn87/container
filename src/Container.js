function Container (context) {
  this.store = {};
}

Container.prototype = {

  param: function (key, value) {
    this.store[key] = { 
      context: this,
      value: value, 
      type: 'param' 
    };
    return this;
  },

  factory: function (key, constructor) {
    this.store[key] = { 
      context: this,
      value: constructor, 
      type: 'factory' 
    };
    return this;
  },

  service: function (key, constructor) {
    this.store[key] = {
      context: this,
      constructor: constructor,
      value: constructor, 
      type: 'service' 
    };
    return this;
  },

  container: function (key, container) {
    for(var id in container.store) {
      this.store[id] = container.store[id]; 
    }
    container.store = this.store;
    this.store[key] = {
      context: container,
      value: container,
      type: "container"
    };
    return this;
  },

  get: function (key, params) {
    var item = this.store[key];
    if(item === undefined) return undefined;
    params = params || {};
    try {
      switch (item.type) {
        case 'service':
          this.store[key].type = 'param';
          this.store[key].value = item.value.call(this, params);
          return this.store[key].value;
        case 'factory':
          return item.value.call(this, params);
        case 'param':
        case 'container':
          return item.value;
      }
    } catch (error) {
      if (this.handler) {
        return this.handler.call(this, error, key, item);
      } else throw error;
    }
  },

  alias: function (key, aliases) {
    var item = this.store[key];
    if (!item) return this;
    var length = aliases.length;
    for(var i=0; i < length; ++i) {
      this.store[aliases[i]] = item;
    }
    return this;
  },

  assemble: function (cb) {
    for(var key in this.store) {
      if(this.store[key].type === 'service') {
        this.get(key);
      }
    }
    if(cb)cb.call(this);
    return this;
  },

  error: function (cb) {
    this.handler = cb;
    return this;
  }

};

module.exports = Container;