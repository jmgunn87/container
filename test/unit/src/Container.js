var assert = require('assert');
var Container = require('./../../../src/Container');

describe("Container", function () {

  var container;

  beforeEach(function () {
    container = new Container();
    container.param('test-key', 'hello');
    container.service('test-service', function (p) {
      return new Object({ value: p['test'] || 100 });
    });
    container.factory('test-factory', function (p) {
      return new Object({ value: p['test'] || 100 });
    });
  });

  describe('#constructor', function () {
    it("initializes a new internal store", function () {
      assert.deepEqual(new Container().store, {});
    });
  });

  describe('#param', function () {
    it('sets a parameter on the container', function () {
      assert.equal(container.store['test-key'].value, 'hello');
    });
    it('stores the container context name under the item entry', function () {
      assert.equal(container.store['test-key'].context, container);
    });
  });

  describe('#service', function () {
    it('defines a new service on the container', function () {
      assert.ok(container.store['test-service'].value);
    });
    xit("preserves the service constructor", function () {
      assert(container.store['test-service'].constructor).toEqual(jasmine.any(Function));
    });
    it('stores the container context name under the item entry', function () {
      assert.equal(container.store['test-service'].context, container);
    });
  });

  describe('#factory', function () {
    it('defines a new factory on the container', function () {
      assert.ok(container.store['test-factory'].value);
    });
    it('stores the container context name under the item entry', function () {
      assert.equal(container.store['test-factory'].context, container);
    });
  });

  describe('#container', function () {
    it("merges in configuration from another container", function () {
      var instance = new Container();
      instance.param("test-merge-param", 1);
      instance.service("test-merge-service", function () { return 1; });
      instance.factory("test-merge-factory", function (p) { return p; });
      container.container("container2", instance);
      assert.equal(container.get("test-merge-param"), 1);
      assert.equal(container.get("test-merge-service", 2), 1);
      assert.equal(container.get("test-merge-factory", 2), 2);
      assert.equal(container.get("container2"), instance);
    });
  });

  describe('#get', function () {
    it("returns undefined when an id does not exist", function () {
      assert.ok(!container.get("null-key"));
    });
    it("returns the same parameter", function () {
      assert.equal(container.get('test-key'), 'hello');
    });
    it("returns the same service", function () {
      var instance = container.get('test-service');
      assert.ok(instance);
      assert(instance.value, 100);
      var instance2 = container.get('test-service', {test:1001});
      assert.ok(instance2);
      assert.notEqual(instance2.value, 1001);
    });
    xit("preserves the service constructor after execution", function () {
      var instance = container.get('test-service');
      assert(container.store['test-service'].constructor).toEqual(jasmine.any(Function));
    });
    it("returns a new object from a factory", function () {
      var instance = container.get('test-factory');
      assert.ok(instance)
      assert.equal(instance.value, 100);
      var instance2 = container.get('test-factory', {test:1001});
      assert.ok(instance2);
      assert.equal(instance2.value, 1001);
    });
    it("can cross-reference objects from other containers", function () {
      var container2 = new Container();
      container2.param("cross", 9);
      container.container("container2", container2);
      assert.equal(container.get("cross"), 9);
      assert.equal(container2.get("cross"), 9);
    });
    xit("catches all errors and defers them to the default error handler if set", function () {
      var handler = { cb: function () {} };
      spyOn(handler, "cb");
      container.handler = handler.cb;
      container.factory("ex", function () { throw "ex"; });
      container.get("ex");
      assert(handler.cb).toHaveBeenCalled();
    });
  });

  describe("#alias", function () {
    it("defines aliases for a given container key/id", function () {
      container.param("alias-param", 9);
      container.alias("alias-param", [
        "alias-param-1",
        "alias-param-2",
        "alias-param-3"
      ]);
      assert.equal(container.get("alias-param-1"), 9);
      assert.equal(container.get("alias-param-2"), 9);
      assert.equal(container.get("alias-param-3"), 9);
    }); 
  });

  describe('#assemble', function () {
    it('constructs all services', function () {
      container.assemble(function () {
        assert.ok(container.store["test-service"]);
        assert.ok(container.store["test-service"].value);
      });
    });
  });

  describe("#error", function () {
    it("defines an error handler for the container", function () {
      container.error(function () {});
      assert.ok(container.handler);
    });
  });

});
